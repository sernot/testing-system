"""Auth endpoints"""

from datetime import datetime, timedelta

from jose import jwt

from fastapi import APIRouter, Depends, status, HTTPException
from fastapi.security.oauth2 import OAuth2PasswordRequestForm

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from passlib.hash import argon2

from avail.schemas.auth import TokenOut
from avail.models.users import User
from avail.db import get_session
from avail.config import get_configuration, Configuration


auth_router = APIRouter(prefix="/login", tags=["Authentication"])


@auth_router.post("/", response_model=TokenOut)
async def login(
    form: OAuth2PasswordRequestForm = Depends(),
    session: AsyncSession = Depends(get_session),
    config: Configuration = Depends(get_configuration),
):
    """
    Log in as an existing user via OAuth2
    """

    statement = select(User).where(User.username == form.username)

    try:

        result: User = (await session.execute(statement)).one()[0]

        if not argon2.verify(form.password, result.hashed_password):
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN, detail="Invalid password"
            )

        return TokenOut(
            access_token=jwt.encode(
                {
                    "username": form.username,
                    "expiration": (datetime.now() + timedelta(minutes=30)).timestamp(),
                },
                config.token_secret,
            ),
            token_type="bearer",
        )

    except NoResultFound as exc:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User does not exist"
        ) from exc
