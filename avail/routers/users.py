"""CRUD endpoints for users"""

from typing import List
from fastapi import APIRouter, Depends, status, Path, HTTPException

from sqlalchemy import select, delete
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import NoResultFound

from passlib.hash import argon2

from avail.schemas.users import User, UserChange, UserNew
from avail.auth import authenticate_user
from avail.db import get_session
from avail.models import users

users_router = APIRouter(prefix="/users", tags=["Users"])

# TODO: an optimization in this case would be to return the user for reusability


async def user_exists(username: str, session: AsyncSession):
    """
    Utility function to check if a user exists
    in the database.
    """
    check_exists_stmt = select(users.User).where(users.User.username == username)
    try:
        (await session.execute(check_exists_stmt)).one()
        return True
    except NoResultFound:
        return False


@users_router.get("/", response_model=List[User])
async def get_all_users(
    user: users.User = Depends(authenticate_user),
    session: AsyncSession = Depends(get_session),
):
    """
    Get all users in the system

    Requires authentication and admin privileges.
    """
    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Admin privileges required"
        )
    statement = select(users.User)
    result = (await session.execute(statement)).all()
    return (row[0] for row in result)


@users_router.get("/{username}", response_model=User)
async def get_user(
    user: users.User = Depends(authenticate_user),
    username: str = Path("User to get"),
    session: AsyncSession = Depends(get_session),
):
    """
    Get info about a user

    Requires authentication.

    A user may only query their own account.

    An admin may query any accounts.

    Will fail if the user does not exist or
    a user attempts to query an illegal account.
    """
    if not user.is_admin and username != user.username:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You may only query your own account",
        )

    try:
        statement = select(users.User).where(users.User.username == username)
        return (await session.execute(statement)).one()[0]
    except NoResultFound as exc:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User does not exist"
        ) from exc


@users_router.post(
    "/{username}", response_model=User, status_code=status.HTTP_201_CREATED
)
async def create_user(
    new_user: UserNew,
    user: users.User = Depends(authenticate_user),
    username: str = Path(description="Username for the new user"),
    session: AsyncSession = Depends(get_session),
):
    """
    Create a user

    Requires authentication and admin priveleges.

    Will fail if the user exists
    """

    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Admin privileges required"
        )

    if await user_exists(username, session):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="User exists"
        )

    user_to_add = users.User(
        username=username,
        is_admin=new_user.is_admin,
        hashed_password=argon2.hash(new_user.password),
    )

    session.add(user_to_add)
    await session.commit()

    return user_to_add


@users_router.patch("/{username}", response_model=User)
async def edit_user(
    info: UserChange,
    user: users.User = Depends(authenticate_user),
    username: str = Path(description="User to edit"),
    session: AsyncSession = Depends(get_session),
):
    """
    Edit a user

    Requires authentication.

    A user may only make changes to their own account.
    The only field a user can change is their password.

    An admin may make changes to any fields for any
    accounts.

    Will fail if the user does not exist or
    a user exists with the specified `new_username`
    """

    if not user.is_admin:
        if info.password is None or username != user.username:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Admin privileges are required to modify anything else than your password",
            )

        info.is_admin = None
        info.new_username = None

    if not await user_exists(username, session):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="User does not exist"
        )

    if info.new_username is not None and await user_exists(info.new_username, session):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User with the new username specified already exists",
        )

    current_record_stmt = select(users.User).where(users.User.username == username)
    current_user: users.User = (await session.execute(current_record_stmt)).one()[0]

    if info.password is not None:
        current_user.hashed_password = argon2.hash(info.password)

    if info.new_username is not None:
        current_user.username = info.new_username

    if info.is_admin is not None:
        current_user.is_admin = info.is_admin

    await session.commit()
    return current_user


@users_router.delete("/{username}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(
    username: str = Path(description="User to delete"),
    user: users.User = Depends(authenticate_user),
    session: AsyncSession = Depends(get_session),
):
    """
    Delete a user and **all associated solutions**

    Requires authentication and admin priveleges.

    Will fail if the user does not exist
    """

    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Admin privileges are required to delete users",
        )

    if not await user_exists(username, session):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="User does not exist"
        )

    statement = delete(users.User).where(users.User.username == username)
    await session.execute(statement)
    await session.commit()
