"""Endpoints for working with exectution servers"""

from typing import List

from fastapi import APIRouter, status, Depends, Path, HTTPException

from sqlalchemy import select, delete
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import IntegrityError

from avail.auth import authenticate_user
from avail.schemas.servers import ServerCreate, ServerOut, ServerInfo
from avail.db import get_session
from avail.models import users, servers

servers_router = APIRouter(prefix="/servers", tags=["Servers"])


@servers_router.get("/", response_model=List[ServerInfo | ServerOut])
async def get_servers(
    user: users.User = Depends(authenticate_user),
    session: AsyncSession = Depends(get_session),
):
    """
    Get info about all execution servers

    Info is returned as ServerInfo, but may
    be returned as ServerOut if capability
    negotiations are not finished.

    Requires authentication and admin privileges
    """
    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Admin privileges required"
        )

    statement = select(servers.Server)
    result = (await session.execute(statement)).all()
    return (row[0] for row in result)


@servers_router.post("/", response_model=ServerOut, status_code=status.HTTP_201_CREATED)
async def create_server(
    server: ServerCreate,
    user: users.User = Depends(authenticate_user),
    session: AsyncSession = Depends(get_session),
):
    """
    Create a server

    Will fail if a server with the same url already exists

    Requires authentication and admin privileges
    """

    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Admin privileges required"
        )

    try:
        server_to_create = servers.Server(
            url=server.url, thread_hint=server.thread_hint, public_key=server.public_key
        )
        session.add(server_to_create)
        await session.commit()
        await session.refresh(server_to_create)
        return server_to_create
    except IntegrityError as exc:
        await session.rollback()
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Server with this url already present",
        ) from exc


@servers_router.delete("/{server_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_server(
    server_id: int = Path(..., description="Server to delete"),
    user: users.User = Depends(authenticate_user),
    session: AsyncSession = Depends(get_session),
):
    """
    Delete a server
    and reschedule all workloads from it

    Requires authentication and admin privileges

    Will **not** fail if the server does not exist
    """

    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Admin privileges required"
        )

    await session.execute(
        delete(servers.Server).where(servers.Server.server_id == server_id)
    )
    await session.commit()
