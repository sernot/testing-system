"""Endpoints for CRUD operations on languages"""

from typing import List

from fastapi import APIRouter, status, Path, Depends, HTTPException

from sqlalchemy import select, delete
from sqlalchemy.ext.asyncio import AsyncSession

from avail.db import get_session
from avail.models import users, languages
from avail.auth import authenticate_user
from avail.schemas.languages import LanguageOut, LanguageFull

languages_router = APIRouter(prefix="/languages", tags=["Languages"])


@languages_router.get("/", response_model=List[LanguageOut])
async def get_languages(session: AsyncSession = Depends(get_session)):
    """
    Get all available languages.
    """

    statement = select(languages.Language)
    result = (await session.execute(statement)).unique().all()
    return (row[0] for row in result)


@languages_router.post(
    "/", status_code=status.HTTP_201_CREATED, response_model=LanguageOut
)
async def create_language(
    language: LanguageFull,
    user: users.User = Depends(authenticate_user),
    session: AsyncSession = Depends(get_session),
):
    """
    Create a language and add support for it to problems.

    Will fail if the language with the name exists.

    Requires authentication and admin privileges.
    """

    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Admin privileges needed"
        )

    language_to_add = languages.Language(
        language_name=language.language_name,
        compilation_command=language.compilation_command,
        version_command=language.version_command,
        compatible_versions=language.compatible_versions,
        main_version=language.main_version,
        required_binaries=[
            languages.BinaryGroup(
                binaries=[
                    languages.Binary(name=binary.name) for binary in group.binaries
                ]
            )
            for group in language.required_binaries
        ],
    )

    session.add(language_to_add)

    await session.commit()
    await session.refresh(language_to_add)
    return language_to_add


@languages_router.delete("/{language_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_language(
    language_id: int = Path(..., description="The language to delete"),
    user: users.User = Depends(authenticate_user),
    session: AsyncSession = Depends(get_session),
):
    """
    Delete a language and stop accepting solution with it.

    Requires authentication and admin priviliges.

    Does not affect existing solutions.

    Will not fail if the language does not exist.
    """

    if not user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Admin privileges needed"
        )

    statement = delete(languages.Language).where(
        languages.Language.language_id == language_id
    )
    await session.execute(statement)
