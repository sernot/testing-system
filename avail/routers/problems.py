"""CRUD endpoints for problems"""

from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import NoResultFound

from fastapi import APIRouter, Depends, status, Path, UploadFile, File, HTTPException
from fastapi.responses import StreamingResponse
from avail.schemas.problems import (
    ProblemOut,
    ProblemFull,
    ProblemNew,
    ProblemChange,
    TestInfo,
)
from avail.auth import authenticate_user
from avail.db import get_session
from avail.models import users, problems


problems_router = APIRouter(prefix="/problems", tags=["Problems and tests"])


@problems_router.get("/", response_model=List[ProblemOut])
async def get_all_problems(session: AsyncSession = Depends(get_session)):
    """
    Get all problems in the system

    Does not require authentication
    """

    statement = select(problems.Problem)
    result = (await session.execute(statement)).all()
    return (row[0] for row in result)


@problems_router.get(
    "/{problem_id}", response_model=ProblemFull, summary="Full info about problem"
)
async def get_problem(
    problem_id: str = Path(..., description="id of the problem to get info about"),
    session: AsyncSession = Depends(get_session),
):
    """
    Get full info about a problem, including its statement

    Does not require authentication

    Will fail if the problem does not exist
    """

    statement = select(problems.Problem).where(
        problems.Problem.problem_id == problem_id
    )
    try:
        result: problems.Problem = (await session.execute(statement)).one()[0]
    except NoResultFound as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Problem does not exists"
        ) from exc
    return result


@problems_router.post(
    "/{problem_id}", response_model=ProblemFull, status_code=status.HTTP_201_CREATED
)
async def create_problem(
    problem: ProblemNew,
    problem_id: str = Path(..., description="id for the new problem"),
    user: int = Depends(authenticate_user),
):
    """
    Create a problem with the specified id and get the newly created problem

    Requires authentication and admin privileges

    Will fail if the problem with that id exists
    or a language to accept does not exist
    """


@problems_router.patch("/{problem_id}", response_model=ProblemFull)
async def edit_problem(
    problem: ProblemChange,
    problem_id: str = Path("id of the problem to edit"),
    user: int = Depends(authenticate_user),
):
    """
    Edit a problem.
    Same as recreating it with new fields, but
    preserves associated solutions

    Requires authentication and admin privileges.

    Will fail if the problem does not exist
    or a language to accept does not exist
    """


@problems_router.delete("/{problem_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_problem(
    problem_id: str = Path(..., description="id of the problem to delete"),
    user: int = Depends(authenticate_user),
):
    """
    Delete a problem and **all associated solutions**

    Requires authentication and admin privileges

    Will fail if the problem does not exist
    """


# Tests


@problems_router.get("/{problem_id}/tests", response_model=List[TestInfo])
async def get_tests(
    problem_id: int = Path(..., description="Problem to get the get the tests of"),
    user: int = Depends(authenticate_user),
):
    """
    Get the test ids of the problem
    in the order they will be executed

    Requires authentication and admin privileges

    Will fail if the problem does not exist
    """


@problems_router.get("/{problem_id}/tests/{test_id}", response_class=StreamingResponse)
async def get_test(
    problem_id: int = Path(..., description="Problem to get the test of"),
    user: int = Depends(authenticate_user),
):
    """
    Get the test data for a test

    Requires authentication and admin privileges

    Will fail if the problem or the test
    do not exist.
    """


@problems_router.post(
    "/{problem_id}/tests/{after_id}",
    status_code=status.HTTP_201_CREATED,
    response_model=TestInfo,
)
async def create_test(
    problem_id: int = Path(..., description="Problem to which a to add a test"),
    after_id: int = Path(..., description="The test to add the new test after"),
    user: int = Depends(authenticate_user),
    file: UploadFile = File(),
):
    """
    Create a test from uploaded file.
    The test will be executed after the test
    with `other_id` and before all tests after
    `other_id`. (immediately after).

    Return the id of the new test.

    May invlaidate other test ids.

    Requires authentication and admin privileges

    Will fail if the problem or the test with `after_id` do not exist
    """


@problems_router.delete(
    "/{problem_id}/tests/{test_id}", status_code=status.HTTP_204_NO_CONTENT
)
async def delete_test(
    problem_id: int = Path(..., description="Problem to change"),
    test_id: int = Path(..., description="Test to delete"),
    user: int = Depends(authenticate_user),
):
    """
    Delete a test

    All test ids more than `test_id` will
    be decremented by one

    Requires authentication and admin privileges

    Will fail if the problem does not exist
    or there are less tests for it than `test_id`
    """
