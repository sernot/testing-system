"""CRUD operations for solutions"""

from typing import List

from fastapi import APIRouter, Depends, status, File, UploadFile, Path
from fastapi.responses import StreamingResponse

from avail.auth import authenticate_user
from avail.schemas.solutions import SolutionChange, SolutionOut, SolutionCreate

solutions_router = APIRouter(prefix="/solutions", tags=["Solutions"])


@solutions_router.get(
    "/{problem_id}/{username}",
    summary="Get all solutions for a problem",
    response_model=List[SolutionOut],
)
async def get_all_solutions(
    problem_id: int = Path(..., description="Problem to query solutions of"),
    username: str = Path("", description="Username which solutions are filtered by"),
    user: int = Depends(authenticate_user),
):
    """
    Get all solutions for a given problem by a user.
    If no user is specified, currently authenticated
    user will be used.

    Requires authentication.

    A user may only view their own solutions.

    An admin may view solutions from any users.

    Will fail if the problem or the user do not
    exist.
    """


@solutions_router.get(
    "/{solution_id}", summary="Get info on one solution", response_model=SolutionOut
)
async def get_solution(
    solution_id: int = Path(..., description="Solution to get info about"),
    user: int = Depends(authenticate_user),
):
    """
    Get a solution. A cheaper way to poll on the status.

    Requires authentication.

    A user may only view their own solutions.

    An admin may view any solutions.

    Will fail if the solution does not exist.
    """


@solutions_router.get(
    "/{solution_id}/source",
    response_class=StreamingResponse,
    summary="Get source code of a solution",
)
async def get_source(
    solutiond_id: int = Path(..., description="Solution to get sources of"),
    user: int = Depends(authenticate_user),
):
    """
    Get the source code of a solution
    as a file.

    Requires authentication.

    A user may only view the source of
    their own solution.

    An admin may view any sources.

    Will fail if the solution does not exist.
    """


@solutions_router.post(
    "/{problem_id}", status_code=status.HTTP_201_CREATED, response_model=SolutionOut
)
async def create_solution(
    solution: SolutionCreate,
    problem_id: str = Path(..., description="Problem to test the solution against"),
    source: UploadFile = File(),
    user: int = Depends(authenticate_user),
):
    """
    Create a new solution and get info about it.

    Requires authentication.

    Will fail if the problem does not exist
    or the language can not be accepted.
    """


@solutions_router.patch("/{solution_id}", response_model=SolutionOut)
async def change_solution_status(
    solution: SolutionChange,
    solution_id: int = Path(..., description="Solution to change"),
    user: int = Depends(authenticate_user),
):
    """
    Change a solutions status manually.

    Requires authentication and admin privileges.

    Will fail if the solution does not exist
    or the new status is invalid
    """
