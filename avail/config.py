"""Utilities to access avail configuration"""

from functools import lru_cache

from pydantic import BaseSettings
from yaml import safe_load


class Configuration(BaseSettings):
    """
    Configuration options
    storage.
    """

    token_secret: str
    database_connection: str


@lru_cache()
def get_configuration():
    """
    Retrieve the configuration options
    of avail. Cheap to use as a
    dependency.
    """
    with open("avail.yml", encoding="utf-8") as file:
        return Configuration(**safe_load(file))
