"""Avail database connection utilities"""

from types import NoneType
from typing import AsyncGenerator
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from avail.config import get_configuration

Base = declarative_base()

engine = create_async_engine(get_configuration().database_connection)
session_fact = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)


async def get_session() -> AsyncGenerator[AsyncSession, NoneType]:
    """
    A dependency to obtain a database session.
    Closed automatically.
    """
    session: AsyncSession = session_fact()
    try:
        yield session
    finally:
        await session.close()
