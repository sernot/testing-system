from sqlalchemy import String, Boolean

from sqlalchemy.orm import Mapped, mapped_column

from avail.db import Base


class User(Base):
    __tablename__ = "Users"

    username: Mapped[str] = mapped_column(String, primary_key=True)
    is_admin: Mapped[bool] = mapped_column(Boolean)
    hashed_password: Mapped[str] = mapped_column(String)
