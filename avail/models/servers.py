from sqlalchemy import String, Integer

from sqlalchemy.orm import Mapped, mapped_column

from avail.db import Base


class Server(Base):
    __tablename__ = "Servers"

    server_id: Mapped[int] = mapped_column(
        Integer, primary_key=True, autoincrement=True
    )
    url: Mapped[str] = mapped_column(String, unique=True)
    public_key: Mapped[str] = mapped_column(String)
    thread_hint: Mapped[int] = mapped_column(Integer)
