from sqlalchemy import String, Integer, ForeignKey, Text

from sqlalchemy.orm import Mapped, mapped_column

from avail.db import Base


class Problem(Base):
    __tablename__ = "Problems"

    problem_id: Mapped[int] = mapped_column(
        Integer, primary_key=True, autoincrement=True
    )
    name: Mapped[str] = mapped_column(String, unique=True)
    statement: Mapped[str] = mapped_column(String)
    language_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("Languages.language_id")
    )


class Test(Base):
    __tablename__ = "Tests"

    problem_id: Mapped[int] = mapped_column(Integer, ForeignKey(Problem.problem_id))
    test_id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    order: Mapped[int] = mapped_column(Integer)
    content: Mapped[str] = mapped_column(Text)
