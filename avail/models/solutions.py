from datetime import datetime

from sqlalchemy import String, Integer, ForeignKey, Text, DateTime

from sqlalchemy.orm import Mapped, mapped_column

from avail.db import Base


class Solution(Base):
    __tablename__ = "Solutions"

    username: Mapped[str] = mapped_column(String, ForeignKey("Users.username"))
    problem_id: Mapped[int] = mapped_column(Integer, ForeignKey("Problems.problem_id"))
    language_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("Languages.language_id")
    )
    source: Mapped[str] = mapped_column(Text)
    status: Mapped[str] = mapped_column(String)
    created_at: Mapped[datetime] = mapped_column(DateTime)
