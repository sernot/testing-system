from sqlalchemy import String, Integer, ForeignKey

from sqlalchemy.orm import Mapped, mapped_column, relationship

from avail.db import Base

# Language definitions will mostly be used for capability
# negotiations, so joining in all relationships is not a performance hit


class Language(Base):
    __tablename__ = "Languages"

    language_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    language_name: Mapped[str] = mapped_column(String)
    compilation_command: Mapped[str] = mapped_column(String)
    version_command: Mapped[str] = mapped_column(String)
    required_binaries: Mapped[list["BinaryGroup"]] = relationship(
        "BinaryGroup", cascade="all, delete-orphan", lazy="joined"
    )
    compatible_versions: Mapped[str] = mapped_column(String)
    main_version: Mapped[str] = mapped_column(String)


class BinaryGroup(Base):
    __tablename__ = "BinaryGroups"

    language_id: Mapped[int] = mapped_column(Integer, ForeignKey(Language.language_id))
    binary_group_id: Mapped[int] = mapped_column(
        Integer, autoincrement=True, primary_key=True
    )
    binaries: Mapped[list["Binary"]] = relationship(
        "Binary", cascade="all, delete-orphan", lazy="joined"
    )


class Binary(Base):
    __tablename__ = "Binaries"

    binary_id: Mapped[int] = mapped_column(
        Integer, primary_key=True, autoincrement=True
    )
    binary_group_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(BinaryGroup.binary_group_id)
    )
    name: Mapped[str] = mapped_column(String)
