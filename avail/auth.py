"""Auth utilities"""

from datetime import datetime

from jose import jwt, JWTError

from fastapi import Depends, status
from fastapi.security.oauth2 import OAuth2PasswordBearer
from fastapi import HTTPException

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import NoResultFound

from avail.db import get_session
from avail.models.users import User
from avail.config import get_configuration, Configuration

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login/")


async def authenticate_user(
    token: str = Depends(oauth2_scheme),
    session: AsyncSession = Depends(get_session),
    config: Configuration = Depends(get_configuration),
) -> User:
    """
    When used as a dependency, returns the user making the request
    Prevents further execution if authentication is invalid
    """
    try:
        payload = jwt.decode(token, config.token_secret)

        username = payload["username"]
        expiration = payload["expiration"]

    except (JWTError, KeyError) as exc:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Invalid token signature or payload",
        ) from exc

    if datetime.fromtimestamp(expiration) < datetime.now():
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Token has expired"
        )

    statement = select(User).where(User.username == username)
    try:
        return (await session.execute(statement)).one()[0]
    except NoResultFound as exc:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="User does not exist"
        ) from exc
