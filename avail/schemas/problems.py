"""Models for CRUD operations with problems"""

from typing import List

from pydantic import BaseModel, Field

name_field = Field(description="Short descriptive problem name")
id_field = Field(description="Tiny problem identifier, unique in the contest")
statement_field = Field(description="Detailed problem statement")
languages_field = Field(description="Languages to accept solutions in")


class ProblemBase(BaseModel):
    """
    Base fields for problems
    """

    name: str | None = name_field


class ProblemIdBase(ProblemBase):
    """
    Base fields for identified problems:
    editing and output
    """

    problem_id: str = id_field


class ProblemFullBase(ProblemBase):
    """
    Base fields for problems with statements:
    creation, full output, editing
    """

    statement: str | None = statement_field
    languages: List[int] | None = languages_field


class ProblemOut(ProblemIdBase):
    """
    Problem data for lists
    """

    name: str = name_field


class ProblemFull(ProblemFullBase, ProblemIdBase):
    """
    Full problem info for display
    """

    name: str = name_field
    statement: str = statement_field
    languages: List[int] = languages_field


class ProblemNew(ProblemFullBase):
    """
    Full problem info for creation
    """

    name: str = name_field
    statement: str = statement_field
    languages: List[int] = languages_field


class ProblemChange(ProblemFullBase):
    """
    Problem editing data.
    """

    new_problem_id: str | None = Field(description="Problem id to move the problem to")


# Tests


class TestInfo(BaseModel):
    """
    Test information data
    """

    test_id: int
