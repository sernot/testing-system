"""Schemas for operations with executions servers"""

from typing import List

from pydantic import BaseModel, Field


class ServerBase(BaseModel):
    """Base server info"""

    url: str = Field(description="Connection url")
    public_key: str = Field(description="Public z85-encoded encryption key")
    thread_hint: int | None = Field(
        "Number of threads expected to be provided by the server"
    )

    class Config:  # pylint: disable=too-few-public-methods
        """
        Enable Server schemas to
        be constructed from orm
        objects.
        """

        orm_mode = True


class ServerCreate(ServerBase):
    """Server connection data"""


class ServerOut(ServerBase):
    """Server connection data with identity"""

    server_id: int = Field(description="Unique identifier of the server")


class ServerInfo(ServerOut):
    """Server connection and workload data"""

    maybe_down: bool = Field(
        description="True if the server may be down."
        "If so, the system does not schedule tasks to it."
        "Reconnection attempts will be made forever"
    )
    effective_threads: int = Field(description="Number of threads the server reported")
    capabilities: List[int] = Field(
        description="List of languages that the server supports"
    )
