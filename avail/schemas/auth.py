"""Schemas for authentication"""

from pydantic import BaseModel


class TokenOut(BaseModel):
    """Oauth2 token"""

    access_token: str
    token_type: str
