"""Models for CRUD operations on solutions"""

from datetime import datetime
from pydantic import BaseModel, Field

status_field = Field(description="Current status of the solution")


class SolutionBase(BaseModel):
    """Shared fields for solutions"""

    language: str = Field(description="Language of the solution code")


class SolutionOut(SolutionBase):
    """Solution info for mass output"""

    username: str = Field(description="User who posted the solution")
    problem_id: int = Field(description="Problem which the solution is tested against")
    solution_id: int = Field(description="Unique identifier of this solution")
    created_at: datetime = Field(description="When the solution was posted")
    status: str = status_field


class SolutionCreate(SolutionBase):
    """Data for creating a solution"""


class SolutionChange(BaseModel):
    """Change the status of the solutions"""

    status: str = status_field
