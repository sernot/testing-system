"""Schemas for CRUD operations on languages"""

from typing import List
from pydantic import BaseModel, Field

name_field = Field(description="Name of the language")
main_version_field = Field(
    description="The version advertised to users: all used versions shall be compatible"
)
required_binaries_field = Field(
    description="""
    Binary groups to run and compile the language.
    A server will be reported as capable of executing
    the language if it has at least one binary from
    all groups.

    Binaries are identified by the name. System
    search paths will be used to find exact
    locations.
    """
)
version_command_field = Field(
    description="Command to obtain the implementation version"
)
compilation_command_field = Field(
    description="Command to compile the source into a native executable"
)
compatible_versions_field = Field(
    description="Version specs for version compatible with the main"
)


class OrmModel(BaseModel):
    """Model with ORM construction support"""

    class Config:  # pylint: disable=too-few-public-methods
        """
        Enable Language schemas to
        be constructed from orm
        objects.
        """

        orm_mode = True


class Binary(OrmModel):
    """Named binary"""

    name: str


class BinaryGroup(OrmModel):
    """Named binary group"""

    binaries: List[Binary]


class LanguageBase(OrmModel):
    """
    Base general fields for languages
    """

    language_name: str | None = name_field
    main_version: str | None = main_version_field

    required_binaries: List[BinaryGroup] | None = required_binaries_field
    version_command: str | None = version_command_field
    compilation_command: str | None = compilation_command_field
    compatible_versions: str | None = compatible_versions_field


class LanguageFull(LanguageBase):
    """
    Base required fields for languages
    """

    language_name: str = name_field
    main_version: str = main_version_field
    required_binaries: List[BinaryGroup] = required_binaries_field
    version_command: str = version_command_field
    compilation_command: str = compilation_command_field
    compatible_versions: str = compatible_versions_field


class LanguageOut(LanguageFull):
    """
    Language output model
    """

    language_id: int = Field(description="Unique identifier for the language")
