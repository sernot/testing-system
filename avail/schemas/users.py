"""Models for CRUD operations with users"""

from pydantic import BaseModel, Field

username_field = Field(description="Unique short identifier for the user")
password_field = Field(description="Password used for auth")
is_admin_field = Field(description="True if user has admin priveleges, false otherwise")


class UserBase(BaseModel):
    """
    Base fields for users
    """

    is_admin: bool | None = is_admin_field

    class Config:  # pylint: disable=too-few-public-methods
        """
        Enable User schemas to
        be constructed from orm
        objects.
        """

        orm_mode = True


class UserFullBase(UserBase):
    """
    Base fields for full user info
    """

    password: str | None = password_field


class User(UserBase):
    """
    User info for output
    """

    username: str = username_field
    is_admin: bool = is_admin_field


class UserNew(UserFullBase):
    """
    Info about a new user
    """

    password: str = password_field
    is_admin: bool = is_admin_field


class UserChange(UserFullBase):
    """
    Info for editing a user
    """

    new_username: str | None = Field(description="New username to move the user to")
