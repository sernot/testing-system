"""Avail application entrypoint"""

from fastapi import FastAPI

from avail.db import engine, Base

from avail.routers.auth import auth_router
from avail.routers.problems import problems_router
from avail.routers.users import users_router
from avail.routers.solutions import solutions_router
from avail.routers.languages import languages_router
from avail.routers.servers import servers_router

tags_metadata = [
    {
        "name": "Users",
        "description": "Operations with users.",
    },
    {
        "name": "Problems and tests",
        "description": """
        Manage problems and tests.

        Solutions will be tested against
        test data sequentially, test ids are
        allocated automatically.

        Tests can be deleted, inserted after
        existing tests and queried for content
        and execution order.
        """,
    },
    {
        "name": "Authentication",
        "description": "Authenticate",
    },
    {
        "name": "Solutions",
        "description": "Create and manage solutions",
    },
    {
        "name": "Languages",
        "description": """
        Manage supported programming languages

        A language is defined by required binaries,
        version and compilation commands and compatible
        implementation versions. Version information
        and required binary list will be used to test
        if execution servers support a language.
        """,
    },
    {
        "name": "Servers",
        "description": """
        Manage plugged in execution servers

        Communication with execution servers is
        encrypted with CurveZMQ. The servers shall be
        configured to accept the public key of the api
        server, and the server has to know the public keys
        of the servers. A connection address is also required.

        The system will keep track of the server capabilities,
        effective execution threads and reconnect continiously
        to servers which are potentially down.
        """,
    },
]

app = FastAPI(
    title="Avail API",
    description="API for interacting with the Avail testing system",
    openapi_tags=tags_metadata,
)


async def init_db():
    """
    Ensure the database is initialized
    """
    async with engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("startup")
async def setup():
    """
    Avail startup routine
    """
    await init_db()


app.include_router(auth_router)
app.include_router(problems_router)
app.include_router(users_router)
app.include_router(solutions_router)
app.include_router(languages_router)
app.include_router(servers_router)
